using System;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Converters.Users;
using API.Models.Server.Users.Exceptions;
using API.Models.Server.Users.Repositories;
using API.Services.Auth;
using API.Services.Errors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model = API.Models.Server.Users;
using View = API.Models.Client.Users;

namespace API.Controllers
{
    [Route("")]
    public sealed class UserController : Controller
    {
        private readonly IUserRepository repository;
        private const int ExpireTimeMinutes = 43200;

        public UserController(IUserRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        /// <summary>
        /// Создаёт нового пользователя
        /// </summary>
        /// <param name="creationInfo">Информация о создаваемом пользователе</param>
        /// <param name="cancellationToken"></param>
        /// <response code="201">Возвращает созданный объект</response>
        /// <response code="400">Если creationInfo == null или DuplicationException</response> 
        [HttpPost]
        [Route("registration")]
        public async Task<IActionResult> CreateUserAsync(View.UserCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (creationInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(creationInfo));
                return BadRequest(error);
            }

            var passwordHash = PasswordEncoder.Encode(creationInfo.Password);
            Model.UserCreationInfo modelCreationInfo;

            try
            {
                modelCreationInfo = UserCreationInfoConverter.Convert(creationInfo, passwordHash);
            }
            catch (Exception)
            {
                var error = ServiceErrorResponses.BodyIsInvalid(nameof(creationInfo));
                return BadRequest(error);
            }

            Model.User modelUser;

            try
            {
                modelUser = await repository.CreateAsync(modelCreationInfo, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (UserDuplicationException)
            {
                var error = ServiceErrorResponses.UserDuplication(creationInfo.Login);
                return BadRequest(error);
            }

            var clientUser = UserConverter.Convert(modelUser);
            return CreatedAtRoute("GetUserRoute", new {login = clientUser.Login}, clientUser);
        }

        [HttpGet]
        [Route("api/v1/user/{login}", Name = "GetUserRoute")]
        public async Task<IActionResult> GetUserByLoginAsync([FromRoute] string login,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (login == null)
            {
                var error = ServiceErrorResponses.RouteIsMissing(nameof(login));
                return BadRequest(error);
            }

            Model.User modelUser;

            try
            {
                modelUser = await repository.GetAsync(login, cancellationToken).ConfigureAwait(false);
            }
            catch (UserNotFoundException)
            {
                var error = ServiceErrorResponses.UserNotFound(login);
                return NotFound(error);
            }

            var clientUser = UserConverter.Convert(modelUser);
            return Ok(clientUser);
        }
        
        [HttpGet]
        [Route("check-auth/{token}")]
        public async Task<IActionResult> GetUserByTokenAsync([FromRoute] string token,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (token == null)
            {
                var error = ServiceErrorResponses.RouteIsMissing(nameof(token));
                return BadRequest(error);
            }
            
            if (!Guid.TryParse(token, out var tokenGuid) || tokenGuid == Guid.Empty)
            {
                var error = ServiceErrorResponses.TokenIsInvalid(nameof(token));
                return BadRequest(error);
            }

            Model.User modelUser;

            try
            {
                modelUser = await repository.GetByTokenAsync(tokenGuid, cancellationToken).ConfigureAwait(false);
            }
            catch (UserNotFoundException)
            {
                var error = ServiceErrorResponses.UserNotFound(token);
                return NotFound(error);
            }

            var clientUser = UserConverter.Convert(modelUser);
            return Ok(clientUser);
        }
        
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> AuthenticateUserAsync(string userLogin, string userPassword, 
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (userLogin == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(userLogin));
                return BadRequest(error);
            }
            
            if (userPassword == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(userPassword));
                return BadRequest(error);
            }

            var passwordHash = PasswordEncoder.Encode(userPassword);
            Model.User modelUser;

            try
            {
                modelUser = await repository.AuthenticateAsync(userLogin, passwordHash, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (AuthenticationException)
            {
                var error = ServiceErrorResponses.UnAuthorized();
                return Unauthorized(error);
            }

            SetCookieValue("token", modelUser.Token.ToString());

            return Redirect("/list.html");
        }
        
        private void SetCookieValue(string key, string value)  
        {
            var option = new CookieOptions
            {
                Expires = DateTime.Now.AddMinutes(ExpireTimeMinutes)
            };

            Response.Cookies.Append(key, value, option);  
        }
        
        [HttpPost]
        [Route("logout")]
        public async Task<IActionResult> DeauthenticateUserAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            var token = HttpContext.Request.Cookies["token"];
            
            if (!Guid.TryParse(token, out var tokenGuid) || tokenGuid == Guid.Empty)
            {
                var error = ServiceErrorResponses.UnAuthorized();
                return Unauthorized(error);
            }

            try
            {
                await repository.DeauthenticateAsync(tokenGuid, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (AuthenticationException)
            {
                var error = ServiceErrorResponses.UnAuthorized();
                return Unauthorized(error);
            }

            Response.Cookies.Delete("token");

            return Redirect("/index.html");
        }
    }
}