using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Converters.WayBills;
using API.Models.Server.WayBills.Exceptions;
using API.Models.Server.WayBills.Repositories;
using API.Services.Errors;
using Microsoft.AspNetCore.Mvc;
using Model = API.Models.Server.WayBills;
using View = API.Models.Client.WayBills;

namespace API.Controllers
{
    [Route("api/v1/waybills")]
    public sealed class WayBillController : Controller
    {
        private readonly IWayBillRepository repository;

        public WayBillController(IWayBillRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateWayBillAsync(View.WayBillCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (creationInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(creationInfo));
                return BadRequest(error);
            }

            Model.WayBillCreationInfo modelCreationInfo;

            try
            {
                modelCreationInfo = WayBillCreationInfoConverter.Convert(creationInfo);
            }
            catch (Exception)
            {
                var error = ServiceErrorResponses.BodyIsInvalid(nameof(creationInfo));
                return BadRequest(error);
            }

            Model.WayBill modelWayBill;

            try
            {
                modelWayBill = await repository.CreateAsync(modelCreationInfo, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (WayBillDuplicationException ex)
            {
                var error = ServiceErrorResponses.WayBillDuplication(ex.Message);
                return BadRequest(error);
            }

            var clientWayBill = WayBillConverter.Convert(modelWayBill);
            return CreatedAtRoute("GetWayBillRoute", new {id = clientWayBill.Id}, clientWayBill);
        }
        
        [HttpGet]
        [Route("{id}", Name = "GetWayBillRoute")]
        public async Task<IActionResult> GetWayBillAsync([FromRoute] string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (id == null)
            {
                var error = ServiceErrorResponses.RouteIsMissing(nameof(id));
                return BadRequest(error);
            }

            if (!Guid.TryParse(id, out var guid))
            {
                var error = ServiceErrorResponses.WayBillIdIsInvalid(nameof(id));
                return BadRequest(error);
            }

            Model.WayBill modelWayBill;

            try
            {
                modelWayBill = await repository.GetAsync(guid, cancellationToken).ConfigureAwait(false);
            }
            catch (WayBillNotFoundException)
            {
                var error = ServiceErrorResponses.WayBillNotFound(id);
                return NotFound(error);
            }

            var clientWayBill = WayBillConverter.Convert(modelWayBill);
            return Ok(clientWayBill);
        }
        
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> SearchPlacesAsync([FromQuery]View.WayBillSearchInfo searchInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var modelSearchInfo = WayBillSearchInfoConverter.Convert(searchInfo ?? new View.WayBillSearchInfo());
            var modelWayBillList = await repository.SearchAsync(modelSearchInfo, cancellationToken).ConfigureAwait(false);
            var clientWayBillList = modelWayBillList
                .Select(WayBillConverter.Convert)
                .ToImmutableList();

            return Ok(clientWayBillList);
        }
        
        //todo создать отдельный контроллер для записей механика
        [HttpPost]
        [Route("mechanic/{id}")]
        public async Task<IActionResult> PatchMechanicAsync([FromRoute] string id,
            [FromBody] View.WayBillMechanicPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (patchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(patchInfo));
                return BadRequest(error);
            }

            var modelPatchInfo = WayBillMechanicPatchInfoConverter.Convert(id, patchInfo);
            Model.WayBill modelWayBill;

            try
            {
                modelWayBill = await repository.MechanicPatchAsync(modelPatchInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (WayBillNotFoundException)
            {
                var error = ServiceErrorResponses.WayBillNotFound(id);
                return NotFound(error);
            }

            var clientWayBill = WayBillConverter.Convert(modelWayBill);
            return Ok(clientWayBill);
        }
        
        //todo создать отдельный контроллер для записей доктора
        [HttpPost]
        [Route("doctor/{id}")]
        public async Task<IActionResult> PatchDoctorAsync([FromRoute] string id,
            [FromBody] View.WayBillDoctorPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (patchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(patchInfo));
                return BadRequest(error);
            }

            var modelPatchInfo = WayBillDoctorPatchInfoConverter.Convert(id, patchInfo);
            Model.WayBill modelWayBill;

            try
            {
                modelWayBill = await repository.DoctorPatchAsync(modelPatchInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (WayBillNotFoundException)
            {
                var error = ServiceErrorResponses.WayBillNotFound(id);
                return NotFound(error);
            }

            var clientWayBill = WayBillConverter.Convert(modelWayBill);
            return Ok(clientWayBill);
        }
    }
}