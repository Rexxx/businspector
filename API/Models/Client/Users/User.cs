using System;

namespace API.Models.Client.Users
{
    public class User
    {
        public string Login { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public DateTime RegisteredAt { get; set; }
    }
}