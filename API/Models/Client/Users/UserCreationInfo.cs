using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace API.Models.Client.Users
{
    /// <summary>
    /// Информация для регистрации пользователя
    /// </summary>
    [DataContract]
    public class UserCreationInfo
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        [DataMember(IsRequired = true)]
        [StringLength(24, MinimumLength=3)]
        public string Login { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [DataMember(IsRequired = true)]
        [StringLength(24, MinimumLength=3)]
        public string Password { get; set; }
        
        /// <summary>
        /// ФИО пользователя
        /// </summary>
        [DataMember(IsRequired = true)]
        [StringLength(128, MinimumLength=3)]
        public string Name { get; set; }
        
        /// <summary>
        /// Номер телефона пользователя
        /// </summary>
        [DataMember(IsRequired = true)]
        [StringLength(18, MinimumLength=7)]
        public string Phone { get; set; }
    }
}