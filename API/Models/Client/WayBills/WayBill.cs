namespace API.Models.Client.WayBills
{
    public class WayBill
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string Organization { get; set; }
        public string Mark { get; set; }
        public string GovNumber { get; set; }
        public int Serial { get; set; }
        public int Number { get; set; }
        public bool MechanicPermission { get; set; }
        public int Change { get; set; }
        public bool[] DoctorPermission { get; set; }
    }
}