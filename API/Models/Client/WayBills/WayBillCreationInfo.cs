using System.Runtime.Serialization;

namespace API.Models.Client.WayBills
{
    public class WayBillCreationInfo
    {
        [DataMember(IsRequired = true)]
        public int Serial { get; set; }
        
        [DataMember(IsRequired = true)]
        public int Number { get; set; }

        [DataMember(IsRequired = true)]
        public string Organization { get; set; }

        [DataMember(IsRequired = true)]
        public string Mark { get; set; }
        
        [DataMember(IsRequired = true)]
        public string GovNumber { get; set; }
    }
}