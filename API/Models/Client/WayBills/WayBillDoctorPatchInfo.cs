using System.Runtime.Serialization;

namespace API.Models.Client.WayBills
{
    public class WayBillDoctorPatchInfo
    {
        [DataMember(IsRequired = true)]
        public int Change { get; set; }
        
        [DataMember(IsRequired = true)]
        public string DoctorPermission { get; set; }
    }
}