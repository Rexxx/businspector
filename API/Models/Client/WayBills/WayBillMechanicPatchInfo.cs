using System.Runtime.Serialization;

namespace API.Models.Client.WayBills
{
    public class WayBillMechanicPatchInfo
    {
        [DataMember(IsRequired = true)]
        public string MechanicPermission { get; set; }
    }
}