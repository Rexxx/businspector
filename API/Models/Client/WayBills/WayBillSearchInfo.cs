namespace API.Models.Client.WayBills
{
    public class WayBillSearchInfo
    {
        public int? Offset { get; set; }
        public int? Limit { get; set; }
        public string MechanicPermission { get; set; }
        public int? Change { get; set; }
        public string[] DoctorPermission { get; set; }
    }
}