using System;
using View = API.Models.Client.Users;
using Model = API.Models.Server.Users;

namespace API.Models.Converters.Users
{
    /// <summary>
    /// Предоставляет методы конвертирования пользователя между серверной и клиентской моделями
    /// </summary>
    public static class UserConverter
    {
        /// <summary>
        /// Переводит подьзователя из серверной модели в клиентскую
        /// </summary>
        /// <param name="modelUser">Пользователь в серверной модели</param>
        /// <returns>Пользователь в клиентской модели</returns>
        public static View.User Convert(Model.User modelUser)
        {
            if (modelUser == null)
            {
                throw new ArgumentNullException(nameof(modelUser));
            }

            var clientUser = new View.User
            {
                Login = modelUser.Login,
                Name = modelUser.Name,
                Role = modelUser.Role.ToString(),
                RegisteredAt = modelUser.RegisteredAt
            };

            return clientUser;
        }
    }
}