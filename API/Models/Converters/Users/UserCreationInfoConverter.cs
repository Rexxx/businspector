using System;
using View = API.Models.Client.Users;
using Model = API.Models.Server.Users;

namespace API.Models.Converters.Users
{
    public static class UserCreationInfoConverter
    {
        public static Model.UserCreationInfo Convert(View.UserCreationInfo clientCreationInfo, string passwordHash)
        {
            if (clientCreationInfo == null)
            {
                throw new ArgumentNullException(nameof(clientCreationInfo));
            }

            if (passwordHash == null)
            {
                throw new ArgumentNullException(nameof(passwordHash));
            }

            if (passwordHash == string.Empty)
            {
                throw new ArgumentException($"{nameof(passwordHash)} can't be empty.");
            }
            
            var modelCreationInfo = new Model.UserCreationInfo(clientCreationInfo.Login, passwordHash, 
                clientCreationInfo.Name, clientCreationInfo.Phone);

            return modelCreationInfo;
        }
    }
}