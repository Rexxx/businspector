using System;
using Model = API.Models.Server.WayBills;
using View = API.Models.Client.WayBills;

namespace API.Models.Converters.WayBills
{
    public static class WayBillConverter
    {
        public static View.WayBill Convert(Model.WayBill wayBill)
        {
            if (wayBill == null)
            {
                throw new ArgumentNullException(nameof(wayBill));
            }

            var clientWayBill = new View.WayBill
            {
                Id = wayBill.Id.ToString(),
                Date = wayBill.Date.ToString("dd.MM.yyyy"),
                Organization = wayBill.Organization,
                Mark = wayBill.Mark,
                GovNumber = wayBill.GovNumber,
                Serial = wayBill.Serial,
                Number = wayBill.Number,
                MechanicPermission = wayBill.MechanicPermission,
                Change = wayBill.Change,
                DoctorPermission = wayBill.DoctorPermission
            };

            return clientWayBill;
        }
    }
}