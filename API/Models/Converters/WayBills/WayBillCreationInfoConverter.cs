using System;
using Model = API.Models.Server.WayBills;
using View = API.Models.Client.WayBills;

namespace API.Models.Converters.WayBills
{
    public class WayBillCreationInfoConverter
    {
        public static Model.WayBillCreationInfo Convert(View.WayBillCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }
            
            var modelCreationInfo = new Model.WayBillCreationInfo(creationInfo.Serial, creationInfo.Number, 
                creationInfo.Organization, creationInfo.Mark, creationInfo.GovNumber);

            return modelCreationInfo;
        }
    }
}