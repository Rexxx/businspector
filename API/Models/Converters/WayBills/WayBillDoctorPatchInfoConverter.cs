using System;
using System.IO;
using Model = API.Models.Server.WayBills;
using View = API.Models.Client.WayBills;

namespace API.Models.Converters.WayBills
{
    public static class WayBillDoctorPatchInfoConverter
    {
        public static Model.WayBillDoctorPatchInfo Convert(string id, View.WayBillDoctorPatchInfo patchInfo)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            if (!Guid.TryParse(id, out var guid) || !bool.TryParse(patchInfo.DoctorPermission, out var doctorPermission))
            {
                throw new InvalidDataException();
            }

            var modelPatchInfo = new Model.WayBillDoctorPatchInfo(guid, doctorPermission, patchInfo.Change);

            return modelPatchInfo;
        } 
    }
}