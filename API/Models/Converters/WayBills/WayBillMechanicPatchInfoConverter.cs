using System;
using System.IO;
using Model = API.Models.Server.WayBills;
using View = API.Models.Client.WayBills;

namespace API.Models.Converters.WayBills
{
    public static class WayBillMechanicPatchInfoConverter
    {
        public static Model.WayBillMechanicPatchInfo Convert(string id, View.WayBillMechanicPatchInfo patchInfo)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            if (!Guid.TryParse(id, out var guid) || !bool.TryParse(patchInfo.MechanicPermission, out var mechanicPermission))
            {
                throw new InvalidDataException();
            }

            var modelPatchInfo = new Model.WayBillMechanicPatchInfo(guid, mechanicPermission);

            return modelPatchInfo;
        } 
    }
}