using System;
using System.Linq;
using Model = API.Models.Server.WayBills;
using View = API.Models.Client.WayBills;

namespace API.Models.Converters.WayBills
{
    public static class WayBillSearchInfoConverter
    {
        public static Model.WayBillSearchInfo Convert(View.WayBillSearchInfo searchInfo)
        {
            if (searchInfo == null)
            {
                throw new ArgumentNullException(nameof(searchInfo));
            }

            bool? mechanicPermission = null;

            if (bool.TryParse(searchInfo.MechanicPermission, out var mp))
            {
                mechanicPermission = mp;
            }

            bool?[] doctorPermission = null;

            if (searchInfo.DoctorPermission != null)
            {
                doctorPermission = searchInfo.DoctorPermission.Select(StringBoolConverter).ToArray();
            }

            var modelSearchInfo = new Model.WayBillSearchInfo
            {
                Limit = searchInfo.Limit,
                Offset = searchInfo.Offset,
                MechanicPermission = mechanicPermission,
                Change = searchInfo.Change,
                DoctorPermission = doctorPermission
            };

            return modelSearchInfo;
        }
        
        private static bool? StringBoolConverter(string boolValue)
        {
            bool? realBool = null;

            if (bool.TryParse(boolValue, out var tmpBool))
            {
                realBool = tmpBool;
            }

            return realBool;
        }
    }
}