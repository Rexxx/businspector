using System;

namespace API.Models.Server.Users.Exceptions
{
    /// <summary>
    /// Исключение, которое возникает при попытке получить несуществующего пользователя
    /// </summary>
    public class UserNotFoundException : Exception
    {
        /// <summary>
        /// Инициализировать экземпляр исключения по логину пользователя
        /// </summary>
        /// <param name="login"></param>
        public UserNotFoundException(string login)
            : base($"User by login \"{login}\" is not found.")
        {
        }
    }
}