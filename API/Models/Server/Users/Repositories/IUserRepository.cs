using System;
using System.Threading;
using System.Threading.Tasks;

namespace API.Models.Server.Users.Repositories
{
    /// <summary>
    /// Интерфейс, описывающий хранилище пользователей
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Создать нового пользователя
        /// </summary>
        /// <param name="creationInfo">Данные для создания нового пользователя</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns>Созданный пользователь</returns>
        Task<User> CreateAsync(UserCreationInfo creationInfo, CancellationToken cancellationToken);

        /// <summary>
        /// Получить пользователя по логину
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns>Пользователь</returns>
        Task<User> GetAsync(string login, CancellationToken cancellationToken);
        
        /// <summary>
        /// Получить пользователя по токену
        /// </summary>
        /// <param name="token">Токен авторизации</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns>Пользователь</returns>
        Task<User> GetByTokenAsync(Guid token, CancellationToken cancellationToken);

        Task<User> AuthenticateAsync(string login, string password, CancellationToken cancellationToken);
        Task DeauthenticateAsync(Guid token, CancellationToken cancellationToken);
    }
}