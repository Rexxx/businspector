using System;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Server.Users.Exceptions;
using MongoDB.Driver;

namespace API.Models.Server.Users.Repositories
{
    public class MongoUserRepository : IUserRepository
    {
        private readonly IMongoCollection<User> users;

        public MongoUserRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("BusInspectorDb");
            users = database.GetCollection<User>("Users");
        }
        
        public Task<User> CreateAsync(UserCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            
            var userWithSameLogin = users.Find(item => item.Login == creationInfo.Login).FirstOrDefault();

            if (userWithSameLogin != null)
            {
                throw new UserDuplicationException(userWithSameLogin.Login);
            }
            
            var now = DateTime.Now;
            var user = new User
            {
                Login = creationInfo.Login,
                PasswordHash = creationInfo.PasswordHash,
                Name = creationInfo.Name,
                Phone = creationInfo.Phone,
                Role = Role.None,
                Token = Guid.Empty,
                RegisteredAt = now
            };
            users.InsertOneAsync(user, cancellationToken: cancellationToken);

            return Task.FromResult(user);
        }

        public Task<User> GetAsync(string login, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var user = users.Find(item => item.Login == login).FirstOrDefault();
            
            if (user == null)
            {
                throw new UserNotFoundException(login);
            }

            return Task.FromResult(user);
        }
        
        public Task<User> GetByTokenAsync(Guid token, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var user = users.Find(item => item.Token == token).FirstOrDefault();
            
            if (user == null)
            {
                throw new UserNotFoundException(token.ToString());
            }

            return Task.FromResult(user);
        }

        public Task<User> AuthenticateAsync(string login, string passwordHash, CancellationToken cancellationToken)
        {
            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }

            if (passwordHash == null)
            {
                throw new ArgumentNullException(nameof(passwordHash));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            
            var user = users.Find(item => item.Login == login).FirstOrDefault();
            
            if (user == null)
            {
                throw new AuthenticationException();
            }

            if (!user.PasswordHash.Equals(passwordHash))
            {
                throw new AuthenticationException();
            }
            
            var token = Guid.NewGuid();
            user.Token = token;
            users.ReplaceOne(item => item.Login == login, user);
            
            return Task.FromResult(user);
        }
        
        public Task DeauthenticateAsync(Guid token, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            var user = users.Find(item => item.Token == token).FirstOrDefault();
            
            if (user == null)
            {
                throw new AuthenticationException();
            }
            
            var emptyToken = Guid.Empty;
            user.Token = emptyToken;
            users.ReplaceOne(item => item.Token == token, user);
            
            return Task.CompletedTask;
        }
    }
}