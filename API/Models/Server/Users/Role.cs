namespace API.Models.Server.Users
{
    public enum Role
    {
        Mechanic,
        Doctor,
        Dispatcher,
        Police,
        Administrator,
        None
    }
}