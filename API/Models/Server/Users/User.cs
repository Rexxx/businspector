using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Server.Users
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public string Login { get; set; }

        [BsonElement("PasswordHash")]
        [BsonRepresentation(BsonType.String)]
        public string PasswordHash { get; set; }
        
        [BsonElement("Name")]
        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }
        
        [BsonElement("Phone")]
        [BsonRepresentation(BsonType.String)]
        public string Phone { get; set; }
        
        [BsonElement("Role")]
        [BsonRepresentation(BsonType.String)]
        public Role Role { get; set; }
        
        [BsonElement("Token")]
        [BsonRepresentation(BsonType.String)]
        public Guid Token { get; set; }

        [BsonElement("RegisteredAt")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime RegisteredAt { get; set; }
    }
}