using System;

namespace API.Models.Server.Users
{
    /// <summary>
    /// Информация для создания пользователя
    /// </summary>
    public class UserCreationInfo
    {
        public string Login { get; }
        public string PasswordHash { get; }
        public string Name { get; }
        public string Phone { get; }

        /// <summary>
        /// Инициализирует новый экземпляр описания для создания пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="passwordHash">Хэш пароля</param>
        /// <param name="name">Хэш пароля</param>
        /// <param name="phone">Хэш пароля</param>
        public UserCreationInfo(string login, string passwordHash, string name, string phone)
        {
            Login = login ?? throw new ArgumentNullException(nameof(login));
            PasswordHash = passwordHash ?? throw new ArgumentNullException(nameof(passwordHash));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));
        }
    }
}