using System;

namespace API.Models.Server.WayBills.Exceptions
{
    public class WayBillDuplicationException : Exception
    {
        public WayBillDuplicationException(int serial, int number)
            : base($"Way bill with serial \"{serial}\" and number \"{number}\" already exists;")
        {
            
        }
    }
}