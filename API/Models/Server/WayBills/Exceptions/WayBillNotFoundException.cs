using System;

namespace API.Models.Server.WayBills.Exceptions
{
    public class WayBillNotFoundException : Exception
    {
        public WayBillNotFoundException(string guid)
            : base($"WayBill with id \"{guid}\" not found.")
        {
            
        }
    }
}