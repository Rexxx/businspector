using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace API.Models.Server.WayBills.Repositories
{
    public interface IWayBillRepository
    {
        Task<WayBill> CreateAsync(WayBillCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<WayBill> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<IReadOnlyList<WayBill>> SearchAsync(WayBillSearchInfo searchInfo, CancellationToken cancellationToken);
        Task<WayBill> MechanicPatchAsync(WayBillMechanicPatchInfo patchInfo, CancellationToken cancellationToken);
        Task<WayBill> DoctorPatchAsync(WayBillDoctorPatchInfo patchInfo, CancellationToken cancellationToken);
    }
}