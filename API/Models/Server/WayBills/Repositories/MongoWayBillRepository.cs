using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Server.Users;
using API.Models.Server.Users.Exceptions;
using API.Models.Server.WayBills.Exceptions;
using MongoDB.Driver;

namespace API.Models.Server.WayBills.Repositories
{
    public class MongoWayBillRepository : IWayBillRepository
    {
        private readonly IMongoCollection<WayBill> wayBills;

        public MongoWayBillRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("BusInspectorDb");
            wayBills = database.GetCollection<WayBill>("WayBills");
        }
        
        public Task<WayBill> CreateAsync(WayBillCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            
            var wayBillWithSameSerialNumber = wayBills
                .Find(item => item.Serial == creationInfo.Serial && item.Number == creationInfo.Number)
                .FirstOrDefault();

            if (wayBillWithSameSerialNumber != null)
            {
                throw new WayBillDuplicationException(wayBillWithSameSerialNumber.Serial, wayBillWithSameSerialNumber.Number);
            }
            
            var now = DateTime.Now;
            var id = Guid.NewGuid();
            var wayBill = new WayBill
            {
                Id = id,
                Date = now,
                Organization = creationInfo.Organization,
                Mark = creationInfo.Mark,
                GovNumber = creationInfo.GovNumber,
                Serial = creationInfo.Serial,
                Number = creationInfo.Number,
                MechanicPermission = false,
                Change = 0,
                DoctorPermission = new bool[2]
            };
            wayBills.InsertOneAsync(wayBill, cancellationToken: cancellationToken);

            return Task.FromResult(wayBill);
        }

        public Task<WayBill> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var wayBill = wayBills.Find(item => item.Id == id).FirstOrDefault();
            
            if (wayBill == null)
            {
                throw new WayBillNotFoundException(id.ToString());
            }

            return Task.FromResult(wayBill);
        }

        public Task<IReadOnlyList<WayBill>> SearchAsync(WayBillSearchInfo searchInfo, CancellationToken cancellationToken)
        {
            if (searchInfo == null)
            {
                throw new ArgumentNullException(nameof(searchInfo));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            
            var search = wayBills.Find(item => true).ToEnumerable();

            if (searchInfo.MechanicPermission != null)
            {
                search = search.Where(item => item.MechanicPermission == searchInfo.MechanicPermission);
            }

            if (searchInfo.DoctorPermission != null && searchInfo.Change != null)
            {
                search = search.Where(item => item.DoctorPermission[item.Change] == 
                                              searchInfo.DoctorPermission[(int)searchInfo.Change]);
            }

            if (searchInfo.Offset != null)
            {
                search = search.Skip(searchInfo.Offset.Value);
            }

            if (searchInfo.Limit != null)
            {
                search = search.Take(searchInfo.Limit.Value);
            }

            var result = search.ToList();
            return Task.FromResult<IReadOnlyList<WayBill>>(result);
        }

        public Task<WayBill> MechanicPatchAsync(WayBillMechanicPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var wayBill = wayBills.Find(item => item.Id == patchInfo.Id).FirstOrDefault();

            if (wayBill == null)
            {
                throw new WayBillNotFoundException(patchInfo.Id.ToString());
            }

            wayBill.MechanicPermission = patchInfo.MechanicPermission;
            wayBills.ReplaceOne(item => item.Id == patchInfo.Id, wayBill);

            return Task.FromResult(wayBill);
        }

        public Task<WayBill> DoctorPatchAsync(WayBillDoctorPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var wayBill = wayBills.Find(item => item.Id == patchInfo.Id).FirstOrDefault();

            if (wayBill == null)
            {
                throw new WayBillNotFoundException(patchInfo.Id.ToString());
            }

            wayBill.DoctorPermission[patchInfo.Change] = patchInfo.DoctorPermission;
            wayBills.ReplaceOne(item => item.Id == patchInfo.Id, wayBill);

            return Task.FromResult(wayBill);
        }
    }
}