using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Server.WayBills
{
    public class WayBill
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }

        [BsonElement("Date")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime Date { get; set; }
        
        [BsonElement("Organization")]
        [BsonRepresentation(BsonType.String)]
        public string Organization { get; set; }
        
        [BsonElement("Mark")]
        [BsonRepresentation(BsonType.String)]
        public string Mark { get; set; }
        
        [BsonElement("GovNumber")]
        [BsonRepresentation(BsonType.String)]
        public string GovNumber { get; set; }
        
        [BsonElement("Serial")]
        [BsonRepresentation(BsonType.Int32)]
        public int Serial { get; set; }

        [BsonElement("Number")]
        [BsonRepresentation(BsonType.Int32)]
        public int Number { get; set; }
        
        [BsonElement("MechanicPermission")]
        [BsonRepresentation(BsonType.Boolean)]
        public bool MechanicPermission { get; set; }
        
        [BsonElement("Change")]
        [BsonRepresentation(BsonType.Int32)]
        public int Change { get; set; } 
        
        [BsonElement("DoctorPermission")]
        public bool[] DoctorPermission { get; set; }
    }
}