using System;

namespace API.Models.Server.WayBills
{
    public class WayBillCreationInfo
    {
        public int Serial { get; }
        public int Number { get; }
        public string Organization { get; }
        public string Mark { get; }
        public string GovNumber { get; }

        public WayBillCreationInfo(int serial, int number, string organization, string mark, string govNumber)
        {
            Serial = serial;
            Number = number;
            Organization = organization ?? throw new ArgumentNullException(nameof(organization));
            Mark = mark ?? throw new ArgumentNullException(nameof(mark));
            GovNumber = govNumber ?? throw new ArgumentNullException(nameof(govNumber));
        }
    }
}