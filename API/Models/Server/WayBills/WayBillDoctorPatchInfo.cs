using System;

namespace API.Models.Server.WayBills
{
    public class WayBillDoctorPatchInfo
    {
        public Guid Id { get; }
        public bool DoctorPermission { get; }
        public int Change { get; }

        public WayBillDoctorPatchInfo(Guid id, bool doctorPermission, int change)
        {
            Id = id;
            DoctorPermission = doctorPermission;
            Change = change;
        }
    }
}