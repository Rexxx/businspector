using System;

namespace API.Models.Server.WayBills
{
    public class WayBillMechanicPatchInfo
    {
        public Guid Id { get; }
        public bool MechanicPermission { get; }

        public WayBillMechanicPatchInfo(Guid id, bool mechanicPermission)
        {
            Id = id;
            MechanicPermission = mechanicPermission;
        }
    }
}