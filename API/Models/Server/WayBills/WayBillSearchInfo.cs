namespace API.Models.Server.WayBills
{
    public class WayBillSearchInfo
    {
        public int? Offset { get; set; }
        public int? Limit { get; set; }
        public bool? MechanicPermission { get; set; }
        public int? Change { get; set; }
        public bool?[] DoctorPermission { get; set; }
    }
}