using System;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using API.Services.Errors;
using Microsoft.AspNetCore.Http;

namespace API.Services.Auth
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate next;
        private IAuthorizator authorizator;

        public AuthMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Cookies["token"];

            if (!Guid.TryParse(token, out var tokenGuid) || tokenGuid == Guid.Empty)
            {
                context.Response.StatusCode = 401;
                await context.Response.WriteAsync(ServiceErrorCodes.InvalidCredentials);
            }

            authorizator = (IAuthorizator)context.RequestServices.GetService(typeof(IAuthorizator));
            
            try
            {
                var user = await authorizator.AuthorizeAsync(tokenGuid, CancellationToken.None);
                context.Items.Add("userLogin", user.Login);
                context.Items.Add("userRole", user.Role);
            }
            catch (AuthenticationException)
            {
                context.Response.Cookies.Delete("token");
                context.Response.StatusCode = 401;
                await context.Response.WriteAsync(ServiceErrorCodes.InvalidCredentials);
            }

            await next.Invoke(context);
        }
    }
}