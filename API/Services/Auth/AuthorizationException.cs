using System;

namespace API.Services.Auth
{
    public class AuthorizationException : Exception
    {
        public AuthorizationException()
            : base("Invalid credentials.")
        {
            
        }
    }
}