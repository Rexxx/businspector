using System;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Server.Users;
using API.Models.Server.Users.Exceptions;
using API.Models.Server.Users.Repositories;

namespace API.Services.Auth
{
    public class Authorizator : IAuthorizator
    {
        private readonly IUserRepository repository;

        public Authorizator(IUserRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<User> AuthorizeAsync(Guid token, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            User user;

            try
            {
                user = await repository.GetByTokenAsync(token, cancellationToken).ConfigureAwait(false);
            }
            catch (UserNotFoundException)
            {
                throw new AuthorizationException();
            }

            return user;
        }
    }
}