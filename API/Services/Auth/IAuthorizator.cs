using System;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Server.Users;

namespace API.Services.Auth
{
    public interface IAuthorizator
    {
        Task<User> AuthorizeAsync(Guid token, CancellationToken cancellationToken);
    }
}