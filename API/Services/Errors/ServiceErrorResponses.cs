using System;
using System.Net;

namespace API.Services.Errors
{
    public static class ServiceErrorResponses
    {
        public static ServiceErrorResponse BodyIsMissing(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Request body is empty.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse BodyIsInvalid(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Request body is invalid.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse RouteIsMissing(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Request route value is empty.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse TokenIsInvalid(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Token is invalid.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse WayBillIdIsInvalid(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "WayBillId is invalid.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse UserDuplication(string userInfo)
        {
            if (userInfo == null)
            {
                throw new ArgumentNullException(nameof(userInfo));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotAcceptable,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotAcceptable,
                    Message = $"User \"{userInfo}\" already exists.",
                    Target = "user"
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse UserNotFound(string userInfo)
        {
            if (userInfo == null)
            {
                throw new ArgumentNullException(nameof(userInfo));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"User \"{userInfo}\" not found.",
                    Target = "user"
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse WayBillDuplication(string msg)
        {
            if (msg == null)
            {
                throw new ArgumentNullException(nameof(msg));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotAcceptable,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotAcceptable,
                    Message = msg,
                    Target = "WayBill"
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse WayBillNotFound(string wayBillInfo)
        {
            if (wayBillInfo == null)
            {
                throw new ArgumentNullException(nameof(wayBillInfo));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"WayBill \"{wayBillInfo}\" not found.",
                    Target = "WayBill"
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse UnAuthorized()
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.Forbidden,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.Unauthorized,
                    Message = "User is not authorized."
                }
            };

            return error;
        }
    }
}