using System;
using API.Models.Server.Users.Repositories;
using API.Models.Server.WayBills.Repositories;
using API.Services.Auth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace API
{
    public class Startup
    {
        private const string DocsRoute = "secret-materials/docs";
        private const string DocName = "BI-API";
        private const string DocTitle = "BusInspector API";
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IAuthorizator, Authorizator>();
            services.AddSingleton<IUserRepository, MongoUserRepository>();
            services.AddSingleton<IWayBillRepository, MongoWayBillRepository>();
            services.AddMvc();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(DocName, new Info
                {
                    Version = "v1",
                    Title = DocTitle,
                    Description = "ASP.NET Core Web API"
                });
                options.IncludeXmlComments($"{AppDomain.CurrentDomain.BaseDirectory}/API.xml");
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            app.UseWhen(context => context.Request.Path.StartsWithSegments("/api/v1"),
                appBuilder => { appBuilder.UseMiddleware<AuthMiddleware>(); });

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();

            app.UseSwagger(options => options.RouteTemplate = $"{DocsRoute}/{{documentName}}/swagger.json");
            app.UseSwaggerUI(options =>
            {
                options.RoutePrefix = DocsRoute;
                options.SwaggerEndpoint($"/{DocsRoute}/{DocName}/swagger.json", DocTitle);
            });
        }
    }
}